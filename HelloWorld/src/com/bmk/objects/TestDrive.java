package com.bmk.objects;

import java.util.Date;

public class TestDrive
{
    public static void main(String[] args)
    {
        Car fusion = new Car("Camry", "Toyota", "Premium",2020,1000);
        Car altima = new Car("Altima","Nissan","2.5s",2014,130000);
        fusion.drive(1000);
        altima.drive(100);
        System.out.println(altima.toString());
        System.out.println(altima);

    }
}

package com.bmk.objects;

public class Car
{
    private String name;
    private String make;
    private String model;
    private int year;
    private int  mileage;

    public Car(String name, String make, String model, int year, int mileage)
    {
        this.name = name;
        this.make = make;
        this.model = model;
        this.year = year;
        this.mileage = mileage;
    }

    /**
     * static methods belong to the class not the object
     * instance method
     */

    public void drive(int milesDriven){
        //class must be implemented by the subclass
        mileage += milesDriven;
        System.out.println("You drove " + milesDriven + " miles.");
    }


    public String toString()
    {
        return "Name: " + name + ",Model: "+ model + ",Make: " +
                make + ",Year: " + year + ",Mileage: " + mileage;
    }
}

package com.bmk.conditional;

import java.util.Scanner;

public class WhileLoop
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        //conditions are not predetermined
        int i = 0;
        while(i <= 10){
            System.out.println(i);
            i++;
        }

        do{

        }
        while (i < 5);
        inputValidation();
    }

    public static void inputValidation(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a number: ");
        String input = scanner.nextLine();
        int num = Integer.parseInt(input);
        while (num != 5){
            System.out.println("You did not enter number 5");
            num = Integer.parseInt(scanner.nextLine());
        }
    }

}

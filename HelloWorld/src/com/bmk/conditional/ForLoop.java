package com.bmk.conditional;

public class ForLoop
{
    public static void main(String[] args)
    {
        stairCase(3);
    }
//even numbers
    public static void evenNumbers(int num){
        for (int i = 1; i <=num; i++)
        {
            if (i % 2 ==0){
                System.out.println(i);
            }

        }

    }
//sum of numbers
    public static int sum (int num){
        int sum = 0;
        for (int i = 0; i <=num; i++){
            sum+=i;

        }
        return sum;
    }

    //staircase challenge
    //right facing staircase
    //inverse staircase
    //right facing inverse staircase

    public static void stairCase(int num){
        for (int i = 0; i <=num; i++)
        {
            for (int j = i; j <=num; j++)
            {
                System.out.print(" *");
            }
            System.out.println();
        }

    }
}

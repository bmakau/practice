package com.bmk.conditional;

import java.util.Scanner;

public class ControlFlow
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number: ");
        String input = scanner.nextLine();
        int num = Integer.parseInt(input);
        System.out.println("You have entered: " + num);
        System.out.println("The answer is + 5: " + num + 5);
        System.out.println("The answer is + 5: " + (num + 5));

        if (num > 5){
            System.out.println("Your "+ num +" is greater than 5");
        }
        else{
            System.out.println("Your " + num + " is less than 5");
        }
        //ternary operators
        System.out.println( (num > 5) ? "greater" : "less");
        //switch statement
        switch (num){
            case 0:
                System.out.println("Your number is 0.");
                break;
            case 5:
                System.out.println("Your number is 5.");
                break;
            case 10:
                System.out.println(" Your number is 10");
                break;
            default:
                System.out.println("Your number is greater 10");

        }
    }
}

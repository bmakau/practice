package com.bmk;

public class VipCustomer extends BankAccount
{

    private int creditLimit;

    public VipCustomer(int id, int accountNumber, String name, int phoneNumber, String email, double balance)
    {
        super(1,99999,"John Doe",99999,"johndoe@gmail.com",100);

    }

    public VipCustomer(int id, int accountNumber,
                       String name, int phoneNumber,
                       String email, double balance,
                       int creditLimit)
    {
        super(id, accountNumber, name, phoneNumber, email, balance);
        this.creditLimit = creditLimit;
    }

    public int getCreditLimit()
    {
        return creditLimit;
    }

    public void setCreditLimit(int creditLimit)
    {
        this.creditLimit = creditLimit;
    }

    @Override
    public double deposits(double amount)
    {
        return super.deposits(amount + 0.1*creditLimit);
    }

    @Override
    public void withdraw(double amount)
    {
        //10% of credit limit

        super.withdraw(amount);
    }
}

package com.bmk;

import java.text.DecimalFormat;

public class BankAccount
{
    private int id;
    private int accountNumber;
    private String name;
    private int phoneNumber;
    private String email;
    private double balance;
    private static final DecimalFormat df = new DecimalFormat("0.00");

    public BankAccount(int id, int accountNumber, String name, int phoneNumber, String email, double balance)
    {
        this.id = id;
        this.accountNumber = accountNumber;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.balance = balance;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return "BankAccount{" +
                "id=" + id +
                ", accountNumber= " + accountNumber +
                ", name='" + name + '\'' +
                ", phoneNumber= " + phoneNumber +
                ", email=' " + email + '\'' +
                ", balance= " + balance +
                '}';
    }

    public int getAccountNumber()
    {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public double getBalance()
    {
        return balance;
    }

    public void setBalance(double balance)
    {
        this.balance = balance;
    }

    public double deposits(double amount){
        balance+=amount;
       return balance;
    }

    public void withdraw(double amount){

        if (balance - amount >= 0){
            balance -= amount;
            System.out.println("You have " + balance +" in your account");
        }else{
            System.out.println("You do not have enough money in your account!");
        }
    }
}

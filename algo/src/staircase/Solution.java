package staircase;

public class Solution
{
    public static void main(String[] args)
    {
        stairCase3(3);
    }

    /**
     *
     * @param num solution for staircase problem
     */
    //left facing staircase
    public static void stairCase1(int num){
        for (int i = 0; i <=num; i++)
        {
            for (int j = 0; j <= i; j++)
            {
                System.out.print(" *");
            }
            System.out.println();
        }

    }
//inverse staircase
    public static void stairCase2(int num){
        for (int i = 0; i<=num; i++)
        {
            for (int j = i; j <= num; j++)
            {
                System.out.print(" *");
            }
            System.out.println();
        }

    }


//right facing staircase
    public static void stairCase3(int num)
    {

        int counter = 0;
        for (int i = 0; i <= num; i++)
        {
            counter++;
            for (int k = num; k >= i; k--)
            {
                System.out.print(" ");
            }
            for (int j = 0; j <= counter - 1; j++)
            {
                System.out.print("*");
            }

            System.out.print("\n");
        }
    }
}
